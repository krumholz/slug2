/*********************************************************************
Copyright (C) 2014 Robert da Silva, Michele Fumagalli, Mark Krumholz
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

#ifdef __INTEL_COMPILER
// Need this to fix a bug in the intel compilers relating to c++11
namespace std
{
     typedef decltype(nullptr) nullptr_t;
}
#endif
#include "constants.H"
#include "slug_nebular.H"
#include "slug_MPI.H"
#include <cmath>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>
#include <boost/multi_array.hpp>
#include <boost/lexical_cast.hpp>
extern "C" {
#include "fitsio.h"
}


typedef boost::multi_array<double, 2> array2d;
typedef boost::multi_array<double, 4> array4d;
typedef std::vector<double>::size_type st;

using namespace std;
using namespace boost;
using namespace boost::algorithm;
using namespace boost::filesystem;

////////////////////////////////////////////////////////////////////////
// The constructor; this version is for a single track
////////////////////////////////////////////////////////////////////////
slug_nebular::
slug_nebular(const char *nebular_dir,
	     const std::vector<double>& lambda_in,
	     const char *trackname,
	     slug_ostreams &ostreams_,
	     const double logU,
	     const double phi,
	     const double z) :
  ostreams(ostreams_),
  lambda_star(lambda_in)
{
  
  // Call generic initialization steps
  fitsfile *fptr = init(nebular_dir, z);

  // Initialize data storage
  array2d ssp_ctm_lum;         // Continuum luminosities of SSPs
  array2d ssp_line_lum;        // Line luminosities of SSPs
  vector<double> cts_ctm_lum;  // Continuum luminosity for constant SFR
  vector<double> cts_line_lum; // Line luminosity for constant SFR
  
  // We have a single track file, so just read cloudy data for it
  read_cloudytab_single(fptr, trackname, logU,
			ssp_ctm_lum, ssp_line_lum,
			cts_ctm_lum, cts_line_lum);

  // Use the tabular data we just read to compute LperQ
  init_LperQ(phi, ssp_ctm_lum, ssp_line_lum, cts_ctm_lum, cts_line_lum);
}


////////////////////////////////////////////////////////////////////////
// Second version of constructor; this one works for track sets
////////////////////////////////////////////////////////////////////////
slug_nebular::
slug_nebular(const char *nebular_dir,
	     const std::vector<double>& lambda_in,
	     const double metallicity,
	     const std::vector<std::string>& trackset_filenames,
	     const std::vector<double>& trackset_metallicity,
	     const ZInterpMethod Z_int_meth,
	     slug_ostreams &ostreams_,
	     const double logU,
	     const double phi,
	     const double z) :
  ostreams(ostreams_),
  lambda_star(lambda_in)
{

  // Call generic initialization steps
  fitsfile *fptr = init(nebular_dir, z);

  // Initialize data storage
  array2d ssp_ctm_lum;         // Continuum luminosities of SSPs
  array2d ssp_line_lum;        // Line luminosities of SSPs
  vector<double> cts_ctm_lum;  // Continuum luminosity for constant SFR
  vector<double> cts_line_lum; // Line luminosity for constant SFR

  // Check for common case where the metallicity we have been given
  // exactly matches one of the metallicities available in the track
  // set; in this case there is no need to interpolate and we can just
  // directly read the cloudy data for that track set into storage
  bool exact_match = false;
  for (vector<double>::size_type i=0; i<trackset_metallicity.size(); i++) {
    if (metallicity == trackset_metallicity[i]) {
      exact_match = true;
      read_cloudytab_single(fptr, trackset_filenames[i].c_str(), logU,
			    ssp_ctm_lum, ssp_line_lum,
			    cts_ctm_lum, cts_line_lum);
      break;
    }
  }

  // If we have not found an exact match, we need to interpolate in
  // metallicity, which requires that we read the cloudy data for all
  // metallicities in this track set
  if (!exact_match) {
    cloudytab_metal_interp(fptr, metallicity, logU,
			   trackset_filenames,
			   trackset_metallicity, Z_int_meth,
			   ssp_ctm_lum, ssp_line_lum,
			   cts_ctm_lum, cts_line_lum);
  }

  // Use the tabular data we just read to compute LperQ
  init_LperQ(phi, ssp_ctm_lum, ssp_line_lum, cts_ctm_lum, cts_line_lum);
}


////////////////////////////////////////////////////////////////////////
// The destructor
////////////////////////////////////////////////////////////////////////
slug_nebular::~slug_nebular() {
  delete ion_filter;
}


////////////////////////////////////////////////////////////////////////
// Initialization routine, called by both forms of the constructor
////////////////////////////////////////////////////////////////////////
fitsfile* slug_nebular::init(const char *nebular_dir, const double z) {

  // If FITS is not enabled, throw error and exit if this is called
#ifndef ENABLE_FITS
  ostreams.slug_err_one << "slug_nebular: nebular emission requires "
			<< "slug be compiled with ENABLE_FITS"
			<< std::endl;
  bailout(1);
#else

  // Set up the filter object we'll use to extract ionizing fluxes
  // from input spectra
  vector<double> lambda_tmp, response;
  lambda_tmp.push_back(constants::lambdaHI);
  ion_filter = new slug_filter(lambda_tmp, response, 0.0, 0.0, true);

  // Open the cloudy tabulation file and read the metadata
  fitsfile *fptr = read_cloudytab_metadata(nebular_dir);

  // Initialize the nebular wavelength grid
  init_neb_wl(z);

  // Return pointer to open fits file
  return fptr;
  
#endif
}


////////////////////////////////////////////////////////////////////////
// Routine to open the cloudy tabulation file and read its metadata
////////////////////////////////////////////////////////////////////////
fitsfile* slug_nebular::read_cloudytab_metadata(const char *nebular_dir) {

#ifdef ENABLE_FITS

  // Open the cloudy tabulation
  path dirname(nebular_dir);
  string cloudy_fname = "cloudy_tab.fits";
  path cloudytab_path = dirname / path(cloudy_fname.c_str());
  fitsfile *fptr;
  int fits_status = 0;
  fits_open_file(&fptr, cloudytab_path.c_str(), READONLY, &fits_status);
  if (fits_status) {
    ostreams.slug_err_one << "unable to open file " << cloudy_fname << std::endl;
    bailout(1);
  }

  // Read the set of times for SSP models
  int anynul;
  int colnum;
  {
    long ntime;
    fits_movabs_hdu(fptr, 2, nullptr, &fits_status);
    fits_get_num_rows(fptr, &ntime, &fits_status);
    if (fits_status) {
      ostreams.slug_err_one << "unable to read required data from "
			    << cloudy_fname << std::endl;
      bailout(1);
    }
    ssp_time.resize(ntime);
    fits_read_col(fptr, TDOUBLE, 1, 1, 1, ntime, nullptr, ssp_time.data(),
		  &anynul, &fits_status);
    if (fits_status) {
      ostreams.slug_err_one << "unable to read required data from "
			    << cloudy_fname << std::endl;
      bailout(1);
    }
  }

  // Read the set of line wavelengths
  {
    char colstr[] = "Line_Wavelength";
    fits_movrel_hdu(fptr, 1, nullptr, &fits_status);
    long nline;
    fits_get_num_rows(fptr, &nline, &fits_status);
    fits_get_colnum(fptr, CASEINSEN, colstr, &colnum, &fits_status);
    line_wl.resize(nline);
    fits_read_col(fptr, TDOUBLE, colnum, 1, 1, nline, nullptr,
		  line_wl.data(), &anynul, &fits_status);
    if (fits_status) {
      ostreams.slug_err_one << "unable to read required data from "
			    << cloudy_fname << std::endl;
      bailout(1);
    }
  }

  // Read the set of continuum wavelengths; safety check that it
  // matches up with the set of rest stellar wavelengths with an
  // offset, and record that offset
  {
    char colstr[] = "Continuum_Wavelength";
    fits_movrel_hdu(fptr, 1, nullptr, &fits_status);
    long nctm;
    fits_get_num_rows(fptr, &nctm, &fits_status);
    vector<double> ctm_wl(nctm);
    fits_get_colnum(fptr, CASEINSEN, colstr, &colnum, &fits_status);
    fits_read_col(fptr, TDOUBLE, colnum, 1, 1, nctm, nullptr,
		  ctm_wl.data(), &anynul, &fits_status);
    if (fits_status) {
      ostreams.slug_err_one << "unable to read required data from "
			    << cloudy_fname << std::endl;
      bailout(1);
    }
    ctm_wl_offset = lambda_star.size() - nctm;
    for (vector<double>::size_type i = 0; i < ctm_wl.size(); i++) {
      if (lambda_star[i + ctm_wl_offset] != ctm_wl[i]) {
	ostreams.slug_err_one << "mistmatch in continuum wavelength "
			      << "grid between nebular and stellar "
			      << "data!" << std::endl;
	bailout(1);
      }
    }
  }
	
  // Now read the set of tracks available in the table
  {
    vector<const char *> track_tab_ptr;
    long nmodel;
    fits_movrel_hdu(fptr, 1, nullptr, &fits_status);
    fits_get_num_rows(fptr, &nmodel, &fits_status);
    track_tab.resize(nmodel);
    track_tab_ptr.resize(nmodel);
    char colstr[] = "Track";
    fits_get_colnum(fptr, CASEINSEN, colstr, &colnum, &fits_status);
    int typecode;
    long repeat, width;
    fits_get_coltype(fptr, colnum, &typecode, &repeat, &width, &fits_status);
    for (long i = 0; i < nmodel; i++) {
      track_tab_ptr[i] = new char[repeat];
    }
    fits_read_col(fptr, TSTRING, colnum, 1, 1, nmodel, nullptr,
		  track_tab_ptr.data(), &anynul, &fits_status);
    for (long i = 0; i < nmodel; i++) {
      track_tab[i] = track_tab_ptr[i]; // Copy to C++ string for convenience
      boost::trim(track_tab[i]);  // Trim whitespace
      delete [] track_tab_ptr[i];
    }
    if (fits_status) {
      ostreams.slug_err_one << "unable to read required data from "
			    << cloudy_fname << std::endl;
      bailout(1);
    }
  }

  // Final step: read the logU values
  {
    char colstr[] = "logU";
    fits_get_colnum(fptr, CASEINSEN, colstr, &colnum, &fits_status);
    logU_tab.resize(track_tab.size());
    fits_read_col(fptr, TDOUBLE, colnum, 1, 1, logU_tab.size(), nullptr,
		  logU_tab.data(), &anynul, &fits_status);
    if (fits_status) {
      ostreams.slug_err_one << "unable to read required data from "
			    << cloudy_fname << std::endl;
      bailout(1);
    }
  }

  // Return the pointer to the now-open fits file
  return fptr;

#else

  return nullptr;
  
#endif
}


////////////////////////////////////////////////////////////////////////
// Read a single track and ionization parameter from cloudy table
////////////////////////////////////////////////////////////////////////
void slug_nebular::
read_cloudytab_single(fitsfile *fptr,
		      const char *trackname,
		      const double logU,
		      array2d& ssp_ctm_lum,
		      array2d& ssp_line_lum,
		      std::vector<double>& cts_ctm_lum,
		      std::vector<double>& cts_line_lum) {

  // FITS required here
#ifdef ENABLE_FITS

  // Preparation: strip directory information from the track file
  // we've been given
  path track_path(trackname);
  string trackfile = track_path.filename().native();

  // Make sure the logU we have given is in range
  if (logU < logU_tab.front() || logU > logU_tab.back()) {
    ostreams.slug_err_one << "log(U) out of range for cloudy "
			  << "tabulation file; allowed range is "
			  << logU_tab.front() << " to " << logU_tab.back()
			  << std::endl;
    bailout(1);
  }

  // Get interpolation weights and indices in log(U)
  vector<int>::size_type logU_idx = 0;
  double logU_wgt = 0;
  for (vector<int>::size_type i = 0; i < logU_tab.size(); i++) {
    if (logU_tab[i] == logU) {
      logU_idx = i;
      logU_wgt = 0;
      break;
    } else if (logU_tab[i] < logU && logU < logU_tab[i+1]) {
      logU_idx = i;
      logU_wgt = (logU - logU_tab[i]) / (logU_tab[i+1] - logU_tab[i]);
      break;
    }
  }

  // Resize data holders
  const vector<double>::size_type ntime = ssp_time.size();
  const vector<double>::size_type nline = line_wl.size();
  const vector<double>::size_type nctm = lambda_star.size() - ctm_wl_offset;
  ssp_ctm_lum.resize(boost::extents[nctm][ntime]);
  ssp_line_lum.resize(boost::extents[nline][ntime]);
  cts_ctm_lum.resize(nctm);
  cts_line_lum.resize(nline);

  // FITS utility variables
  int fits_status = 0;
  int colnum = 0;
  int anynul = 0;
  
  // Read set of tracks from cloudy table; this requires some C-style
  // string manipulatio since that is what FITS deals with
  vector<string> track_tab;
  vector<const char *> track_tab_ptr;
  long nmodel;
  {
    fits_movabs_hdu(fptr, 5, nullptr, &fits_status);
    fits_get_num_rows(fptr, &nmodel, &fits_status);
    track_tab.resize(nmodel);
    char colstr[] = "Track";
    fits_get_colnum(fptr, CASEINSEN, colstr, &colnum, &fits_status);
    int typecode;
    long repeat, width;
    fits_get_coltype(fptr, colnum, &typecode, &repeat, &width, &fits_status);
    track_tab_ptr.resize(nmodel);
    track_tab.resize(nmodel);
    for (long i = 0; i < nmodel; i++) {
      track_tab_ptr[i] = new char[width+1];
    }
    fits_read_col(fptr, TSTRING, colnum, 1, 1, nmodel, nullptr,
		  track_tab_ptr.data(), &anynul, &fits_status);
    for (long i = 0; i < nmodel; i++) {
      track_tab[i] = track_tab_ptr[i];
      delete [] track_tab_ptr[i];
    }
    for (auto it : track_tab) {
      boost::trim(it);  // Trim whitespace
    }
    if (fits_status) {
      ostreams.slug_err_one
	<< "unable to read required data from cloudy table"
	<< std::endl;
      bailout(1);
    }
  }
  
  // Find the indices of the track we're looking for
  vector<vector<int>::size_type> idx;
  for (vector<int>::size_type i = 0; i < track_tab.size(); i++) {
    if (track_tab[i] == trackfile) {
      idx.push_back(i);
    }
  }
  if (idx.size() == 0) {
    ostreams.slug_err_one << "unable to find track " << trackfile
			  << " in cloudy table" << std::endl;
    bailout(1);
  }
  
  // Read line and continuum luminosities from correct table row
  {
    char colstr[] = "Ctm_Lum_Gal";
    fits_get_colnum(fptr, CASEINSEN, colstr, &colnum, &fits_status);
    fits_read_col(fptr, TDOUBLE, colnum, idx[logU_idx], 1, cts_ctm_lum.size(),
		  nullptr, cts_ctm_lum.data(), &anynul, &fits_status);
  }
  {
    char colstr[] = "Line_Lum_Gal";
    fits_get_colnum(fptr, CASEINSEN, colstr, &colnum, &fits_status);
    fits_read_col(fptr, TDOUBLE, colnum, idx[logU_idx], 1, cts_line_lum.size(),
		  nullptr, cts_line_lum.data(), &anynul, &fits_status);
  }
  {
    char colstr[] = "Ctm_Lum_Clu";
    fits_get_colnum(fptr, CASEINSEN, colstr, &colnum, &fits_status);
    fits_read_col(fptr, TDOUBLE, colnum, idx[logU_idx], 1, 
      ssp_ctm_lum.num_elements(), nullptr, ssp_ctm_lum.data(),
      &anynul, &fits_status);
  }
  {
    char colstr[] = "Line_Lum_Clu";
    fits_get_colnum(fptr, CASEINSEN, colstr, &colnum, &fits_status);
    fits_read_col(fptr, TDOUBLE, colnum, idx[logU_idx], 1, 
      ssp_line_lum.num_elements(), nullptr, ssp_line_lum.data(),
      &anynul, &fits_status);
  }
  if (fits_status) {
    ostreams.slug_err_one << "unable to read from cloudy tabulation"
			  << std::endl;
    bailout(1);
  }

  // In case where we need to interpolate in log(U), read the data
  // for the next logU value into temporary arrays and then average
  if (logU_wgt != 0) {
    {
      char colstr[] = "Ctm_Lum_Gal";
      vector<double> cts_ctm_lum_tmp(cts_ctm_lum.size());
      fits_get_colnum(fptr, CASEINSEN, colstr, &colnum, &fits_status);
      fits_read_col(fptr, TDOUBLE, colnum, idx[logU_idx+1], 1,
		    cts_ctm_lum_tmp.size(), nullptr,
		    cts_ctm_lum_tmp.data(), &anynul, &fits_status);
      for (long i = 0; i < nctm; i++) {
	cts_ctm_lum[i] = (1 - logU_wgt) * cts_ctm_lum[i] +
	  logU_wgt * cts_ctm_lum_tmp[i];
      }
    }
    {
      char colstr[] = "Line_Lum_Gal";
      vector<double> cts_line_lum_tmp(cts_line_lum.size());
      fits_get_colnum(fptr, CASEINSEN, colstr, &colnum, &fits_status);
      fits_read_col(fptr, TDOUBLE, colnum, idx[logU_idx+1], 1,
		    cts_line_lum_tmp.size(), nullptr,
		    cts_line_lum_tmp.data(), &anynul, &fits_status);
      for (long i = 0; i < nline; i++) {
	cts_line_lum[i] = (1 - logU_wgt) * cts_line_lum[i] +
	  logU_wgt * cts_line_lum_tmp[i];
      }
    }
    {
      char colstr[] = "Ctm_Lum_Clu";
      fits_get_colnum(fptr, CASEINSEN, colstr, &colnum, &fits_status);
      array2d ssp_ctm_lum_tmp;
      ssp_ctm_lum_tmp.resize(boost::extents[ntime][nctm]);
      fits_read_col(fptr, TDOUBLE, colnum, idx[logU_idx], 1,
		    ssp_ctm_lum_tmp.size(), nullptr,
		    ssp_ctm_lum_tmp.data(), &anynul, &fits_status);
      for (vector<double>::size_type i = 0; i < ntime; i++) {
	for (vector<double>::size_type j = 0; j < nctm; j++) {
	  ssp_ctm_lum[i][j] = (1 - logU_wgt) * ssp_ctm_lum[i][j] +
	    logU_wgt * ssp_ctm_lum_tmp[i][j];
	}
      }
    }
    {
      char colstr[] = "Line_Lum_Clu";
      fits_get_colnum(fptr, CASEINSEN, colstr, &colnum, &fits_status);
      array2d ssp_line_lum_tmp;
      ssp_line_lum_tmp.resize(boost::extents[ntime][nline]);
      fits_read_col(fptr, TDOUBLE, colnum, idx[logU_idx], 1,
		    ssp_line_lum_tmp.size(), nullptr,
		    ssp_line_lum_tmp.data(), &anynul, &fits_status);
      for (vector<double>::size_type i = 0; i < ntime; i++) {
	for (vector<double>::size_type j = 0; j < nline; j++) {
	  ssp_line_lum[i][j] = (1 - logU_wgt) * ssp_line_lum[i][j] +
	    logU_wgt * ssp_line_lum_tmp[i][j];
	}
      }
    }
  }

#else
  // Throw error message if called without FITS
  ostreams.slug_err_one << "slug_nebular: nebular emission requires "
			<< "slug be compiled with ENABLE_FITS"
			<< std::endl;
  bailout(1);
#endif
}

////////////////////////////////////////////////////////////////////////
// Generate cloudy tables by interpolting in metallicity
////////////////////////////////////////////////////////////////////////
void slug_nebular::
cloudytab_metal_interp(fitsfile *fptr,
		       const double Z,
		       const double logU,
		       const vector<std::string>& trackset_filenames,
		       const vector<double>& trackset_metallicity,
		       const ZInterpMethod Z_int_meth,
		       array2d& ssp_ctm_lum,
		       array2d& ssp_line_lum,
		       vector<double>& cts_ctm_lum,
		       vector<double>& cts_line_lum) {

  // Prepare temporary storage
  const vector<double>::size_type nZ = trackset_metallicity.size();
  vector<array2d> ssp_ctm_lum_Z(nZ);
  vector<array2d> ssp_line_lum_Z(nZ);
  vector<vector<double> > cts_ctm_lum_Z(nZ);
  vector<vector<double> > cts_line_lum_Z(nZ);

  // Read cloudy data for each metallicity
  for (vector<double>::size_type i = 0; i < trackset_filenames.size(); i++) {
    read_cloudytab_single(fptr, trackset_filenames[i].c_str(), logU,
			  ssp_ctm_lum_Z[i], ssp_line_lum_Z[i],
			  cts_ctm_lum_Z[i], cts_line_lum_Z[i]);
  }

  // Build an interpolator of the requested type to interpolate in
  // metallicity
  const gsl_interp_type* Z_interp_type;
  switch (Z_int_meth) {
  case Z_LINEAR: {
    Z_interp_type = gsl_interp_linear;
    break;
  }
  case Z_AKIMA: {
    if (nZ >= gsl_interp_type_min_size(gsl_interp_akima))
      Z_interp_type = gsl_interp_akima;
    else
      Z_interp_type = gsl_interp_linear;
    break;
  }
#if GSLVERSION >= 2
  case Z_STEFFEN: {
    if (nZ >= gsl_interp_type_min_size(gsl_interp_steffen))
      Z_interp_type = gsl_interp_steffen;
    else
      Z_interp_type = gsl_interp_linear;
    break;
  }
#endif
  default: {
    ostreams.slug_err_one << "slug_nebular constructor invoked with"
			  << " invalid Z interpolation method" << endl;
    bailout(1);
  }
  }
  gsl_interp *interp = gsl_interp_alloc(Z_interp_type, nZ);
  gsl_interp_accel *acc = gsl_interp_accel_alloc();

  // Allocate memory to hold final result
  const vector<double>::size_type ntime = ssp_time.size();
  const vector<double>::size_type nline = line_wl.size();
  const vector<double>::size_type nctm = lambda_star.size() - ctm_wl_offset;
  ssp_ctm_lum.resize(boost::extents[nctm][ntime]);
  ssp_line_lum.resize(boost::extents[nline][ntime]);
  cts_ctm_lum.resize(nctm);
  cts_line_lum.resize(nline);
  
  // We now have to loop over every element of the cloudy tables --
  // every individual time and wavelength -- and interpolate it in
  // metallicity using the interpolator we've just built. The arrays
  // for which this needs to happen are: (1) ssp models, continuum
  // emission, (2) ssp models, line emission, (3) continuous star
  // formation models, continuum emission, (4) continuous star
  // formation models, line emission.
  vector<double> Zdat(nZ);
  for (vector<double>::size_type i = 0; i < nctm; i++) {
    for (vector<double>::size_type j = 0; j < ntime; j++) {
      
      // SSP, continuum
      for (vector<double>::size_type k = 0; k < nZ; k++) {
	Zdat[k] = ssp_ctm_lum_Z[k][i][j];
      }
      gsl_interp_init(interp, trackset_metallicity.data(),
		      Zdat.data(), nZ);
      ssp_ctm_lum[i][j] = gsl_interp_eval(interp,
					  trackset_metallicity.data(),
					  Zdat.data(), Z, acc);
    }
    
    // Continuous SF, continuum
    for (vector<double>::size_type k = 0; k < nZ; k++) {
      Zdat[k] = cts_ctm_lum_Z[k][i];
    }
    gsl_interp_init(interp, trackset_metallicity.data(),
		    Zdat.data(), nZ);
    cts_ctm_lum[i] = gsl_interp_eval(interp,
				     trackset_metallicity.data(),
				     Zdat.data(), Z, acc);
  }
  for (vector<double>::size_type i = 0; i < nline; i++) {
    for (vector<double>::size_type j = 0; j < ntime; j++) {
      
      // SSP, line
      for (vector<double>::size_type k = 0; k < nZ; k++) {
	Zdat[k] = ssp_line_lum_Z[k][i][j];
      }
      gsl_interp_init(interp, trackset_metallicity.data(),
		      Zdat.data(), nZ);
      ssp_line_lum[i][j] = gsl_interp_eval(interp,
					   trackset_metallicity.data(),
					   Zdat.data(), Z, acc);
    }
    
    // Continuous SF, continuum
    for (vector<double>::size_type k = 0; k < nZ; k++) {
      Zdat[k] = cts_line_lum_Z[k][i];
    }
    gsl_interp_init(interp, trackset_metallicity.data(),
		    Zdat.data(), nZ);
    cts_line_lum[i] = gsl_interp_eval(interp,
				      trackset_metallicity.data(),
				      Zdat.data(), Z, acc);
  }

  // Delete the GSL interpolation helpers
  gsl_interp_free(interp);
  gsl_interp_accel_free(acc);
}

////////////////////////////////////////////////////////////////////////
// Interpolate a spectrum from the stellar to the nebular grid
////////////////////////////////////////////////////////////////////////
vector<double>
slug_nebular::interp_stellar(const vector<double> &L_lambda_star,
			     const st offset) 
  const {

  // Find the part of the nebular grid that lies within the range of
  // the input stellar spectrum
  st ptr1, ptr2;
  if (L_lambda_star.size() == lambda_star.size()) {
    // Input spectrum covers full stellar wavelength grid
    assert(offset == 0);
    ptr1 = 0;
    ptr2 = lambda_neb.size();
  } else {
    // Input spectrum is smaller than full stellar wavelength grid
    ptr1=0;
    while (lambda_neb[ptr1] < lambda_star[offset]) ptr1++;
    ptr2=ptr1+1;
    while (lambda_neb[ptr2] <= lambda_star[offset+L_lambda_star.size()-1]) {
      ptr2++;
      if (ptr2 == lambda_neb.size()) break;
    }
  }

  // Create array to return
  vector<double> L_lambda_neb(ptr2-ptr1);

  // Loop over nebular grid
  unsigned int ptr = offset;
  for (st i=ptr1; i<ptr2; i++) {

    if (lambda_neb[i] == lambda_star[ptr]) {

      // This point on the nebular grid matches one on the stellar
      // grid, so just copy
      L_lambda_neb[i-ptr1] = L_lambda_star[ptr-offset];
      ptr++;

    } else {

      // This point on nebular grid is between two points on the
      // stellar grid, so linearly interpolate
      double wgt = log(lambda_neb[i]/lambda_star[ptr-1]) / 
	log(lambda_star[ptr]/lambda_star[ptr-1]);
      L_lambda_neb[i-ptr1] = pow(L_lambda_star[ptr-offset-1], 1.0-wgt) *
	pow(L_lambda_star[ptr-offset], wgt);

    }
  }

  // Return
  return L_lambda_neb;
}


////////////////////////////////////////////////////////////////////////
// Compute nebular spectrum from ionizing luminosity
////////////////////////////////////////////////////////////////////////
vector<double> 
slug_nebular::get_neb_spec(const double QH0,
			   const double age) const {

  // Allocate memory for result
  vector<double> L_lambda(lambda_neb.size(), 0.0);

  // Decide which spectrum to use
  if (age < 0.0) {

    // Continuous SF spectrum
    for (unsigned int i=0; i<lambda_neb.size(); i++) {
      L_lambda[i] = LperQ_cts[i]*QH0;
    }

  } else if (age < ssp_time[0]) {

    // SSP, age < smallest age we have
    for (unsigned int i=0; i<lambda_neb.size(); i++) {
      L_lambda[i] = LperQ_ssp[0][i]*QH0;
    }

  } else if (age < ssp_time.back()) {

    // SSP, inside our age grid, so interpolate

    // Find neighboring ages and get weight between them
    const double dt = ssp_time[2] - ssp_time[1];
    const unsigned int idx = (age - ssp_time[0]) / dt;
    const double wgt = (age - ssp_time[idx]) / dt;

    // Add interpolated spectrum
    for (unsigned int i=0; i<lambda_neb.size(); i++) {
      L_lambda[i] = ((1 - wgt) * LperQ_ssp[idx][i] +
        wgt * LperQ_ssp[idx+1][i]) * QH0;
    }
  }

  // Return
  return L_lambda;
}

////////////////////////////////////////////////////////////////////////
// Compute nebular spectrum from input spectrum
////////////////////////////////////////////////////////////////////////
vector<double> 
slug_nebular::get_neb_spec(const vector<double>& L_lambda,
			   const double age) const {

  // Get ionizing photon flux from input spectrum
  double QH0 = ion_filter->compute_photon_lum(lambda_star, L_lambda);

  // Return value from get_spectrum routine with ionizing flux
  return get_neb_spec(QH0, age);
}

////////////////////////////////////////////////////////////////////////
// Compute total stellar + nebular spectrum
////////////////////////////////////////////////////////////////////////
vector<double>
slug_nebular::get_tot_spec(const vector<double>& L_lambda,
			   const double age) const {

  // Get nebular contribution to spectrum
  vector<double> neb_spec = get_neb_spec(L_lambda, age);

  // Add stellar and nebular contributions, then return
  return add_stellar_nebular_spec(L_lambda, neb_spec);
}


////////////////////////////////////////////////////////////////////////
// Add stellar and nebular spectra
////////////////////////////////////////////////////////////////////////
vector<double>
slug_nebular::
add_stellar_nebular_spec(const vector<double>& L_lambda_star,
			 const vector<double>& L_lambda_neb,
			 const st off_star,
			 const st off_neb) const {

  // Copy nebular spectrum to output holder
  vector<double> tot_spec(L_lambda_neb);
  
  // Get stellar spectrum interpolated onto nebular grid
  vector<double> star_spec = interp_stellar(L_lambda_star, off_star);

  // Add stellar contribution at wavelengths longer than 912 Angstrom
  st i=0;
  while (lambda_neb[i+off_neb] < constants::lambdaHI) i++;
  for ( ; i<star_spec.size(); i++) tot_spec[i] += star_spec[i];

  // Return
  return tot_spec;
}


////////////////////////////////////////////////////////////////////////
// Set up the nebular wavelength grid
////////////////////////////////////////////////////////////////////////
void slug_nebular::init_neb_wl(const double z) {
  
  // Start by setting nebular grids to match the stellar grid
  lambda_neb = lambda_star;

  // Add grid points around lines
  for (vector<double>::size_type i=0; i<line_wl.size(); i++) {

    // Get central wavelength and line width for this line
    double wlcen = line_wl[i];
    double lw = wlcen * linewidth / constants::c;

    // If we're off the wavelength table, skip this line
    if ((wlcen < lambda_star[0]) || 
	(wlcen > lambda_star[lambda_star.size()-1])) continue;

    // Insert gridpoints around the line center
    for (unsigned int k=0; k<ngrid_line; k++) {
      double wl = wlcen + line_extent * lw * 
	(2.0 * k/(ngrid_line-1.0) - 1.0);
      for (unsigned int l=0; l<lambda_neb.size(); l++) {
	if (wl < lambda_neb[l]) {
	  // Skip if this wavelength is already in the grid
	  if (wl != lambda_neb[l-1]) 
	    lambda_neb.insert(lambda_neb.begin() + l, wl);
	  break;
	}
      }
    }
  }

  // Set up the observed-frame wavelength grids
  lambda_neb_obs = lambda_neb;
  for (unsigned int i=0; i<lambda_neb.size(); i++)
    lambda_neb_obs[i] *= 1.0 + z;
}

////////////////////////////////////////////////////////////////////////
// Compute L/Q ratio on nebular grid from cloudy table data
////////////////////////////////////////////////////////////////////////
void slug_nebular::init_LperQ(const double phi,
			      const array2d& ssp_ctm_lum,
			      const array2d& ssp_line_lum,
			      const vector<double>& cts_ctm_lum,
			      const vector<double>& cts_line_lum) {

  // Resize output holders
  LperQ_cts.resize(lambda_neb.size());
  LperQ_ssp.resize(boost::extents[ssp_time.size()][lambda_neb.size()]);

  // Build interpolator to interpolate continuum emission to nebular
  // grid
  const vector<double>::size_type nwl = lambda_star.size() - ctm_wl_offset;
#if GSLVERSION >= 2
  gsl_interp *interp = gsl_interp_alloc(gsl_interp_steffen, nwl);
#else
  gsl_interp *interp = gsl_interp_alloc(gsl_interp_akima, nwl);
#endif
  gsl_interp_accel *acc = gsl_interp_accel_alloc();

  // Interpolate continuum to nebular grid
  const double *wl_in = lambda_star.data() + ctm_wl_offset;
  {
    const double *cts_dat = cts_ctm_lum.data();
    gsl_interp_init(interp, wl_in, cts_dat, nwl);
    for (vector<double>::size_type i = 0; i < lambda_neb.size(); i++) {
      if (lambda_neb[i] < wl_in[0]) {
	LperQ_cts[i] = 0;
      } else {
	LperQ_cts[i] = phi * gsl_interp_eval(interp, wl_in, cts_dat,
					     lambda_neb[i], acc);
      }
    }
  }

  // Repeat for each time in the SSP models
  {
    for (vector<double>::size_type j = 0; j < ssp_time.size(); j++) {
      vector<double> ssp_dat(nwl);
      for (vector<double>::size_type i = 0; i < nwl; i++) {
	ssp_dat[i] = ssp_ctm_lum[i][j];
      }
      gsl_interp_init(interp, wl_in, ssp_dat.data(), nwl);
      for (vector<double>::size_type i = 0; i < lambda_neb.size(); i++) {
	if (lambda_neb[i] < wl_in[0]) {
	  LperQ_ssp[j][i] = 0;
	} else {
	  LperQ_ssp[j][i] = phi * gsl_interp_eval(interp, wl_in,
						  ssp_dat.data(),
						  lambda_neb[i],
						  acc);
	}
      }
    }
  }

  // De-allocate gsl machinery
  gsl_interp_free(interp);
  gsl_interp_accel_free(acc);

  // Now loop over lines to add line profiles to nebular grid
  for (vector<double>::size_type i = 0; i < line_wl.size(); i++) {

    // Line central wavelength and dispersion
    const double wl = line_wl[i];
    const double sigma_wl = wl * linewidth / constants::c;

    // Loop over nebular grid wavelengths
    for (vector<double>::size_type j = 0; j < lambda_neb.size(); j++) {

      // Gaussian profile factor
      const double x = (lambda_neb[j] - wl) / sigma_wl;
      const double gaussfac = 1.0 / (sqrt(2.0 * M_PI) * sigma_wl) *
	exp( -x*x / 2);

      // Continuous case
      LperQ_cts[j] += gaussfac * phi * cts_line_lum[i];

      // SSP cases
      for (vector<double>::size_type k = 0; k < ssp_time.size(); k++) {
	LperQ_ssp[k][j] += gaussfac * phi * ssp_line_lum[i][k];
      }
      
    }
    
  }
				     
}
