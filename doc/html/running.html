<!DOCTYPE html>

<html lang="en" data-content_root="./">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" /><meta name="viewport" content="width=device-width, initial-scale=1" />

    <title>Running a SLUG simulation &#8212; slug 2.0 documentation</title>
    <link rel="stylesheet" type="text/css" href="_static/pygments.css?v=fa44fd50" />
    <link rel="stylesheet" type="text/css" href="_static/basic.css?v=686e5160" />
    <link rel="stylesheet" type="text/css" href="_static/alabaster.css?v=27fed22d" />
    <script src="_static/documentation_options.js?v=60dbed4a"></script>
    <script src="_static/doctools.js?v=9bcbadda"></script>
    <script src="_static/sphinx_highlight.js?v=dc90522c"></script>
    <link rel="index" title="Index" href="genindex.html" />
    <link rel="search" title="Search" href="search.html" />
    <link rel="next" title="Parameter Specification" href="parameters.html" />
    <link rel="prev" title="Compiling and Installing SLUG" href="compiling.html" />
   
  <link rel="stylesheet" href="_static/custom.css" type="text/css" />
  

  
  

  </head><body>
  

    <div class="document">
      <div class="documentwrapper">
        <div class="bodywrapper">
          

          <div class="body" role="main">
            
  <section id="running-a-slug-simulation">
<h1>Running a SLUG simulation<a class="headerlink" href="#running-a-slug-simulation" title="Link to this heading">¶</a></h1>
<section id="basic-serial-runs">
<h2>Basic Serial Runs<a class="headerlink" href="#basic-serial-runs" title="Link to this heading">¶</a></h2>
<p>Once SLUG is compiled, running a simulation is extremely simple. The first step, which is not required but makes life a lot simpler, is to set the environment variable <code class="docutils literal notranslate"><span class="pre">SLUG_DIR</span></code> to the directory where you have installed SLUG. If you are using a <code class="docutils literal notranslate"><span class="pre">bash</span></code>-like shell, the syntax for this is:</p>
<div class="highlight-rest notranslate"><div class="highlight"><pre><span></span>export SLUG_DIR = /path/to/slug
</pre></div>
</div>
<p>while for a <code class="docutils literal notranslate"><span class="pre">csh</span></code>-like shell, it is:</p>
<div class="highlight-rest notranslate"><div class="highlight"><pre><span></span>setenv SLUG_DIR /path/to/slug
</pre></div>
</div>
<p>This is helpful because SLUG needs a lot of input data, and if you don’t set this variable, you will have to manually specify where to find it.</p>
<p>Next, to run on a single processor, just do:</p>
<div class="highlight-rest notranslate"><div class="highlight"><pre><span></span>./bin/slug param/filename.param
</pre></div>
</div>
<p>where <code class="docutils literal notranslate"><span class="pre">filename.param</span></code> is the name of a parameter file, formatted as specified in <a class="reference internal" href="parameters.html#sec-parameters"><span class="std std-ref">Parameter Specification</span></a>. The code will write a series of output files as described in <a class="reference internal" href="output.html#sec-output"><span class="std std-ref">Output Files and Format</span></a>.</p>
</section>
<section id="thread-based-parallelism">
<h2>Thread-Based Parallelism<a class="headerlink" href="#thread-based-parallelism" title="Link to this heading">¶</a></h2>
<p>If you have more than one core at your disposal, you can also run SLUG in parallel using threads, via the command line:</p>
<div class="highlight-rest notranslate"><div class="highlight"><pre><span></span>python ./bin/slug.py param/filename.param
</pre></div>
</div>
<p>This called a python script that automatically divides up the Monte Carlo trials you have requested between the available processors, then consolidates the output so that it looks the same as if you had run a single-processor job. The python script allows fairly fine-grained control of the parallelism. It accepts the following command line arguments (not an exhaustive list – do <code class="docutils literal notranslate"><span class="pre">python</span> <span class="pre">./bin/slug.py</span> <span class="pre">--help</span></code> for the full list):</p>
<ul class="simple">
<li><p><code class="docutils literal notranslate"><span class="pre">-n</span> <span class="pre">NPROC,</span> <span class="pre">--nproc</span> <span class="pre">NPROC</span></code>: this parameter specifies the number of simultaneous SLUG processes to run. It defaults to the number of cores present on the machine where the code is running.</p></li>
<li><p><code class="docutils literal notranslate"><span class="pre">-b</span> <span class="pre">BATCHSIZE,</span> <span class="pre">--batchsize</span> <span class="pre">BATCHSIZE</span></code>: this specifies how to many trials to do per SLUG process. It defaults to the total number of trials requested divided by the total number of processes, rounded up, so that only one SLUG process is run per processor. <em>Rationale</em>: The default behavior is optimal from the standpoint of minimizing the overhead associated with reading data from disk, etc. However, if you are doing a very large number of runs that are going to require hours, days, or weeks to complete, and you probably want the code to checkpoint along the way. In that case it is probably wise to set this to a value smaller than the default in order to force output to be dumped periodically.</p></li>
<li><p><code class="docutils literal notranslate"><span class="pre">-nc,</span> <span class="pre">--noconsolidate</span></code>: by default the <code class="docutils literal notranslate"><span class="pre">slug.py</span></code> script will take all the outputs produced by the parallel runs and consolidate them into single output files, matching what would have been produced had the code been run in serial mode. If set, this flag suppresses that behavior, and instead leaves the output as a series of files whose root names match the model name given in the parameter file, plus the extension <code class="docutils literal notranslate"><span class="pre">_pPPPPP_nNNNNN</span></code>, where the digits <code class="docutils literal notranslate"><span class="pre">PPPPP</span></code> give the number of the processor that produces that file, and the digits <code class="docutils literal notranslate"><span class="pre">NNNNN</span></code> give the run number on that processor. <em>Rationale</em>: normally consolidation is convenient. However, if the output is very large, this may produce undesirably bulky files. Furthermore, if one is doing a very large number of simulations over an extended period, and the <code class="docutils literal notranslate"><span class="pre">slug.py</span></code> script is going to be run multiple times (e.g., due to wall clock limits on a cluster), it may be preferable to leave the files unconsolidated until all runs have been completed.</p></li>
</ul>
</section>
<section id="mpi-based-parallelism">
<h2>MPI-Based Parallelism<a class="headerlink" href="#mpi-based-parallelism" title="Link to this heading">¶</a></h2>
<p>SLUG can also run in parallel on distributed-memory architectures using MPI. To use MPI, you must first compile the code with MPI support – see <a class="reference internal" href="compiling.html#ssec-compiling"><span class="std std-ref">Compiling</span></a>. Then to start an MPI-parallel computation, do:</p>
<div class="highlight-rest notranslate"><div class="highlight"><pre><span></span>mpirun -np N bin/slug param/filename.param
</pre></div>
</div>
<p>where <cite>N</cite> is the number of parallel processes to run. In this mode each MPI process will write its own output files, which will be named as <cite>MODELNAME_XXXX_FILETYPE.EXT</cite> where <cite>MODELNAME</cite> is the model name specified in the parameter file (see <a class="reference internal" href="parameters.html#sec-parameters"><span class="std std-ref">Parameter Specification</span></a>), <cite>XXXX</cite> is the process number of the process that wrote the file, <cite>FILETYPE</cite> is the type of output file (see <a class="reference internal" href="output.html#sec-output"><span class="std std-ref">Output Files and Format</span></a>), and <cite>EXT</cite> is the extension specifying the file format (see <a class="reference internal" href="output.html#sec-output"><span class="std std-ref">Output Files and Format</span></a>).</p>
<p>If it is desirable to do so, the output files produced by an MPI run can be combined into a single output file using the <code class="docutils literal notranslate"><span class="pre">consolidate.py</span></code> script in the <code class="docutils literal notranslate"><span class="pre">tools</span></code> subdirectory.</p>
<p>Note that full parallel computation is only available under MPI implementations that support the MPI 3.0 standard or later. Earlier versions of MPI allow MPI functionality for SLUG in library mode (see <a class="reference internal" href="library.html#sec-library-mode"><span class="std std-ref">Using SLUG as a Library</span></a>), but do not allow MPI parallel runs of the slug executable.</p>
</section>
<section id="checkpointing-and-restarting">
<h2>Checkpointing and Restarting<a class="headerlink" href="#checkpointing-and-restarting" title="Link to this heading">¶</a></h2>
<p>When running a large number of trials, it is often desirable to checkpoint the calculation, i.e., to write intermediate outputs rather than waiting until the entire calculation is done to write. SLUG can checkpoint after a specified number of trials; this number is controlled by the <cite>checkpoint_interval</cite> parameter (see <a class="reference internal" href="parameters.html#sec-parameters"><span class="std std-ref">Parameter Specification</span></a>). Checkpoint files are are named as <cite>MODELNAME_chkYYYY_FILETYPE.EXT</cite> (or <cite>MODELNAME_XXXX_chkYYYY_FILETYPE.EXT</cite> for MPI runs) where <cite>YYYY</cite> is the number of the checkpoint, starting at 0. Checkpoints are valid output files with some added information – see <a class="reference internal" href="output.html#ssec-checkpoint-files"><span class="std std-ref">Checkpoint Files</span></a> for details.</p>
<p>To restart a run from checkpoints, just give the command line option <cite>–restart</cite>, for example:</p>
<div class="highlight-rest notranslate"><div class="highlight"><pre><span></span>mpirun -np N bin/slug param/filename.param --restart
</pre></div>
</div>
<p>SLUG will automatically search for checkpoint files (using the file names specified in <cite>filename.param</cite>), determine how many trials they contain, and resume the run to complete any remaining trials neede to reach the target number specified in the parameter file.</p>
<p>As with MPI runs, the output checkpoint files run can be combined into a single output file using the <code class="docutils literal notranslate"><span class="pre">consolidate.py</span></code> script in the <code class="docutils literal notranslate"><span class="pre">tools</span></code> subdirectory.</p>
</section>
</section>


          </div>
          
        </div>
      </div>
      <div class="sphinxsidebar" role="navigation" aria-label="Main">
        <div class="sphinxsidebarwrapper">
<h1 class="logo"><a href="index.html">slug</a></h1>









<search id="searchbox" style="display: none" role="search">
    <div class="searchformwrapper">
    <form class="search" action="search.html" method="get">
      <input type="text" name="q" aria-labelledby="searchlabel" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" placeholder="Search"/>
      <input type="submit" value="Go" />
    </form>
    </div>
</search>
<script>document.getElementById('searchbox').style.display = "block"</script><h3>Navigation</h3>
<ul class="current">
<li class="toctree-l1"><a class="reference internal" href="license.html">License</a></li>
<li class="toctree-l1"><a class="reference internal" href="getting.html">Getting SLUG</a></li>
<li class="toctree-l1"><a class="reference internal" href="intro.html">Introduction to SLUG</a></li>
<li class="toctree-l1"><a class="reference internal" href="compiling.html">Compiling and Installing SLUG</a></li>
<li class="toctree-l1 current"><a class="current reference internal" href="#">Running a SLUG simulation</a><ul>
<li class="toctree-l2"><a class="reference internal" href="#basic-serial-runs">Basic Serial Runs</a></li>
<li class="toctree-l2"><a class="reference internal" href="#thread-based-parallelism">Thread-Based Parallelism</a></li>
<li class="toctree-l2"><a class="reference internal" href="#mpi-based-parallelism">MPI-Based Parallelism</a></li>
<li class="toctree-l2"><a class="reference internal" href="#checkpointing-and-restarting">Checkpointing and Restarting</a></li>
</ul>
</li>
<li class="toctree-l1"><a class="reference internal" href="parameters.html">Parameter Specification</a></li>
<li class="toctree-l1"><a class="reference internal" href="pdfs.html">Probability Distribution Functions</a></li>
<li class="toctree-l1"><a class="reference internal" href="output.html">Output Files and Format</a></li>
<li class="toctree-l1"><a class="reference internal" href="filters.html">Filters and Filter Data</a></li>
<li class="toctree-l1"><a class="reference internal" href="slugpy.html">slugpy – The Python Helper Library</a></li>
<li class="toctree-l1"><a class="reference internal" href="cloudy.html">cloudy_slug: An Automated Interface to cloudy</a></li>
<li class="toctree-l1"><a class="reference internal" href="bayesphot.html">bayesphot: Bayesian Inference for Stochastic Stellar Populations</a></li>
<li class="toctree-l1"><a class="reference internal" href="cluster_slug.html">cluster_slug: Bayesian Inference of Star Cluster Properties</a></li>
<li class="toctree-l1"><a class="reference internal" href="sfr_slug.html">sfr_slug: Bayesian Inference of Star Formation Rates</a></li>
<li class="toctree-l1"><a class="reference internal" href="tests.html">Test Problems</a></li>
<li class="toctree-l1"><a class="reference internal" href="library.html">Using SLUG as a Library</a></li>
<li class="toctree-l1"><a class="reference internal" href="acknowledgements.html">Contributors and Acknowledgements</a></li>
</ul>

<div class="relations">
<h3>Related Topics</h3>
<ul>
  <li><a href="index.html">Documentation overview</a><ul>
      <li>Previous: <a href="compiling.html" title="previous chapter">Compiling and Installing SLUG</a></li>
      <li>Next: <a href="parameters.html" title="next chapter">Parameter Specification</a></li>
  </ul></li>
</ul>
</div>








        </div>
      </div>
      <div class="clearer"></div>
    </div>
    <div class="footer">
      &#169;2014, Mark Krumholz, Michele Fumagalli, et al..
      
      |
      Powered by <a href="https://www.sphinx-doc.org/">Sphinx 8.1.3</a>
      &amp; <a href="https://alabaster.readthedocs.io">Alabaster 1.0.0</a>
      
      |
      <a href="_sources/running.rst.txt"
          rel="nofollow">Page source</a>
    </div>

    

    
  </body>
</html>