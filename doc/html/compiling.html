<!DOCTYPE html>

<html lang="en" data-content_root="./">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" /><meta name="viewport" content="width=device-width, initial-scale=1" />

    <title>Compiling and Installing SLUG &#8212; slug 2.0 documentation</title>
    <link rel="stylesheet" type="text/css" href="_static/pygments.css?v=fa44fd50" />
    <link rel="stylesheet" type="text/css" href="_static/basic.css?v=686e5160" />
    <link rel="stylesheet" type="text/css" href="_static/alabaster.css?v=27fed22d" />
    <script src="_static/documentation_options.js?v=60dbed4a"></script>
    <script src="_static/doctools.js?v=9bcbadda"></script>
    <script src="_static/sphinx_highlight.js?v=dc90522c"></script>
    <link rel="index" title="Index" href="genindex.html" />
    <link rel="search" title="Search" href="search.html" />
    <link rel="next" title="Running a SLUG simulation" href="running.html" />
    <link rel="prev" title="Introduction to SLUG" href="intro.html" />
   
  <link rel="stylesheet" href="_static/custom.css" type="text/css" />
  

  
  

  </head><body>
  

    <div class="document">
      <div class="documentwrapper">
        <div class="bodywrapper">
          

          <div class="body" role="main">
            
  <section id="compiling-and-installing-slug">
<h1>Compiling and Installing SLUG<a class="headerlink" href="#compiling-and-installing-slug" title="Link to this heading">¶</a></h1>
<section id="dependencies">
<h2>Dependencies<a class="headerlink" href="#dependencies" title="Link to this heading">¶</a></h2>
<p>The core SLUG program requires</p>
<ul class="simple">
<li><p>The <a class="reference external" href="http://www.boost.org/">Boost C++ libraries</a></p></li>
<li><p>The <a class="reference external" href="http://www.gnu.org/software/gsl/">GNU scientific library</a> (version 2.x preferred, code can be compiled with version 1.x – see below)</p></li>
</ul>
<p>In addition, the following are required for some functionality, but not for the core code:</p>
<ul class="simple">
<li><p>The <a class="reference external" href="http://heasarc.gsfc.nasa.gov/fitsio/fitsio.html">cfitsio library</a> (required for FITS capabilities, which are needed for certain track sets and for nebular emission calculations)</p></li>
<li><p>An implementation of <a class="reference external" href="http://mpi-forum.org/">MPI</a> (required for MPI support; for full functionality, the implementation must support the MPI 3.0 or later standard)</p></li>
</ul>
<p>Compilation will be easiest if you install the required libraries such that the header files are included in your <code class="docutils literal notranslate"><span class="pre">CXX_INCLUDE_PATH</span></code> (for Boost) and <code class="docutils literal notranslate"><span class="pre">C_INCLUDE_PATH</span></code> (for GSL, cfitsio, and MPI) and the compiled object files are in your <code class="docutils literal notranslate"><span class="pre">LIBRARY_PATH</span></code>. Alternately, you can manually specify the locations of these files by editing the Makefiles – see below. The cfitsio library is optional, and is only required if you want the ability to write FITS output. To compile without it, use the flag <code class="docutils literal notranslate"><span class="pre">FITS=DISABLE_FITS</span></code> when calling <code class="docutils literal notranslate"><span class="pre">make</span></code> (see below). The MPI libraries are required only for MPI capability, which is not enabled by default; see <a class="reference internal" href="library.html#ssec-mpi-support"><span class="std std-ref">Using SLUG as a Library with MPI-Enabled Codes</span></a> for an explanation of these capabilities and how to enable them. Note that SLUG uses some Boost libraries that must be built separately (see the Boost documentation on how to build and install Boost libraries).</p>
<p>In addition to the core dependencies, slugpy, the python helper library requires:</p>
<ul class="simple">
<li><p><a class="reference external" href="http://www.numpy.org/">numpy</a></p></li>
<li><p><a class="reference external" href="http://www.scipy.org/">scipy</a></p></li>
<li><p><a class="reference external" href="http://www.astropy.org/">astropy</a> (optional, only required for FITS capabilities)</p></li>
</ul>
<p>Finally, the cloudy coupling capability requires:</p>
<ul class="simple">
<li><p><a class="reference external" href="http://nublado.org">cloudy</a></p></li>
</ul>
<p>This is only required performing cloudy runs, and is not required for any other part of SLUG.</p>
</section>
<section id="compiling">
<span id="ssec-compiling"></span><h2>Compiling<a class="headerlink" href="#compiling" title="Link to this heading">¶</a></h2>
<p>If you have Boost in your <code class="docutils literal notranslate"><span class="pre">CXX_INCLUDE_PATH</span></code>, GSL in your <code class="docutils literal notranslate"><span class="pre">C_INCLUDE_PATH</span></code>, and (if you’re using it) cfitsio in your <code class="docutils literal notranslate"><span class="pre">C_INCLUDE_PATH</span></code>, and the compiled libraries for each of these in your <code class="docutils literal notranslate"><span class="pre">LIBRARY_PATH</span></code> environment variables, and your system is running either MacOSX or Linux, you should be able to compile simply by doing:</p>
<div class="highlight-rest notranslate"><div class="highlight"><pre><span></span>make
</pre></div>
</div>
<p>from the main <code class="docutils literal notranslate"><span class="pre">slug</span></code> directory.</p>
<p>To compile in debug mode, do:</p>
<div class="highlight-rest notranslate"><div class="highlight"><pre><span></span>make debug
</pre></div>
</div>
<p>instead.</p>
<p>To enable MPI support, do:</p>
<div class="highlight-rest notranslate"><div class="highlight"><pre><span></span>make MPI=ENABLE_MPI
</pre></div>
</div>
<p>In addition, you may need to specify the names of your preferred MPI
C++ compiler by setting the variable <code class="docutils literal notranslate"><span class="pre">MACH_MPICXX</span></code> in your
machine-specific makefile – see <a class="reference internal" href="#ssec-machine-makefiles"><span class="std std-ref">Machine-Specific Makefiles</span></a>. The
Makefiles contain reasonable guesses, but since MPI compiler names are
much less standardized than general compiler names, you may need to
supply yours rather than relying on the default.</p>
<p>If you are compiling using GSL version 1.x or without cfitsio, you
must specify these options when compiling. If you are using version
1.x of the GSL, do:</p>
<div class="highlight-rest notranslate"><div class="highlight"><pre><span></span>make GSLVERSION=1
</pre></div>
</div>
<p>To compile without FITS support, do:</p>
<div class="highlight-rest notranslate"><div class="highlight"><pre><span></span>make FITS=DISABLE_FITS
</pre></div>
</div>
<p>Note that SLUG is written in C++17, and requires some C++17 features,
so it may not work with older C++ compilers.</p>
</section>
<section id="machine-specific-makefiles">
<span id="ssec-machine-makefiles"></span><h2>Machine-Specific Makefiles<a class="headerlink" href="#machine-specific-makefiles" title="Link to this heading">¶</a></h2>
<p>You can manually specify the compiler flags to be used for you machine
by creating a file named <code class="docutils literal notranslate"><span class="pre">Make.mach.MACHINE_NAME</span></code> in the <code class="docutils literal notranslate"><span class="pre">src</span></code>
directory, and then doing:</p>
<div class="highlight-rest notranslate"><div class="highlight"><pre><span></span>make MACHINE=MACHINE_NAME
</pre></div>
</div>
<p>An example machine-specific file, <code class="docutils literal notranslate"><span class="pre">src/Make.mach.avatar</span></code> is
included in the repository. You can also override or reset any
compilation flag you want by editing the file
<code class="docutils literal notranslate"><span class="pre">src/Make.config.override</span></code>.</p>
</section>
<section id="note-on-boost-naming-and-linking-issues">
<h2>Note on Boost Naming and Linking Issues<a class="headerlink" href="#note-on-boost-naming-and-linking-issues" title="Link to this heading">¶</a></h2>
<p>The <a class="reference external" href="http://www.boost.org/">Boost</a> libraries have a somewhat complex
history of naming conventions (see this <a class="reference external" href="https://stackoverflow.com/questions/2293962/boost-libraries-in-multithreading-aware-mode">stackoverflow discussion thread</a>). As
a result, depending on your platform and where you got your Boost
libraries and how you compiled them, the libraries names may or may
not have names that end in <code class="docutils literal notranslate"><span class="pre">-mt</span></code> (indicating multithreading
support). There is unfortunately no easy way to guess whether this tag
will be present or not in the Boost installation on any particular
system, so the <code class="docutils literal notranslate"><span class="pre">slug</span></code> makefiles contain defaults that are guesses
based on some of the most common boost installations (e.g., the
macports version of Boost has the <code class="docutils literal notranslate"><span class="pre">-mt</span></code> tag, so the default on
Darwin is to include it). If you find that your attempted compilation
fails at the linking stage with an error like:</p>
<div class="highlight-rest notranslate"><div class="highlight"><pre><span></span>ld: library not found for -lboost_system-mt
</pre></div>
</div>
<p>or:</p>
<div class="highlight-rest notranslate"><div class="highlight"><pre><span></span>ld: library not found for -lboost_system
</pre></div>
</div>
<p>but you are confident that you have boost installed and the path
correctly set, you can try adding or removing the <code class="docutils literal notranslate"><span class="pre">-mt</span></code> flag. To do
so, edit the file <code class="docutils literal notranslate"><span class="pre">src/Make.config.override</span></code> and add the line:</p>
<div class="highlight-rest notranslate"><div class="highlight"><pre><span></span>MACH_BOOST_TAG          = -mt
</pre></div>
</div>
<p>(to turn the <code class="docutils literal notranslate"><span class="pre">-mt</span></code> tag on) or:</p>
<div class="highlight-rest notranslate"><div class="highlight"><pre><span></span>MACH_BOOST_TAG          =
</pre></div>
</div>
<p>(to turn the <code class="docutils literal notranslate"><span class="pre">-mt</span></code> tag off). Then try compiling again.</p>
</section>
</section>


          </div>
          
        </div>
      </div>
      <div class="sphinxsidebar" role="navigation" aria-label="Main">
        <div class="sphinxsidebarwrapper">
<h1 class="logo"><a href="index.html">slug</a></h1>









<search id="searchbox" style="display: none" role="search">
    <div class="searchformwrapper">
    <form class="search" action="search.html" method="get">
      <input type="text" name="q" aria-labelledby="searchlabel" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" placeholder="Search"/>
      <input type="submit" value="Go" />
    </form>
    </div>
</search>
<script>document.getElementById('searchbox').style.display = "block"</script><h3>Navigation</h3>
<ul class="current">
<li class="toctree-l1"><a class="reference internal" href="license.html">License</a></li>
<li class="toctree-l1"><a class="reference internal" href="getting.html">Getting SLUG</a></li>
<li class="toctree-l1"><a class="reference internal" href="intro.html">Introduction to SLUG</a></li>
<li class="toctree-l1 current"><a class="current reference internal" href="#">Compiling and Installing SLUG</a><ul>
<li class="toctree-l2"><a class="reference internal" href="#dependencies">Dependencies</a></li>
<li class="toctree-l2"><a class="reference internal" href="#compiling">Compiling</a></li>
<li class="toctree-l2"><a class="reference internal" href="#machine-specific-makefiles">Machine-Specific Makefiles</a></li>
<li class="toctree-l2"><a class="reference internal" href="#note-on-boost-naming-and-linking-issues">Note on Boost Naming and Linking Issues</a></li>
</ul>
</li>
<li class="toctree-l1"><a class="reference internal" href="running.html">Running a SLUG simulation</a></li>
<li class="toctree-l1"><a class="reference internal" href="parameters.html">Parameter Specification</a></li>
<li class="toctree-l1"><a class="reference internal" href="pdfs.html">Probability Distribution Functions</a></li>
<li class="toctree-l1"><a class="reference internal" href="output.html">Output Files and Format</a></li>
<li class="toctree-l1"><a class="reference internal" href="filters.html">Filters and Filter Data</a></li>
<li class="toctree-l1"><a class="reference internal" href="slugpy.html">slugpy – The Python Helper Library</a></li>
<li class="toctree-l1"><a class="reference internal" href="cloudy.html">cloudy_slug: An Automated Interface to cloudy</a></li>
<li class="toctree-l1"><a class="reference internal" href="bayesphot.html">bayesphot: Bayesian Inference for Stochastic Stellar Populations</a></li>
<li class="toctree-l1"><a class="reference internal" href="cluster_slug.html">cluster_slug: Bayesian Inference of Star Cluster Properties</a></li>
<li class="toctree-l1"><a class="reference internal" href="sfr_slug.html">sfr_slug: Bayesian Inference of Star Formation Rates</a></li>
<li class="toctree-l1"><a class="reference internal" href="tests.html">Test Problems</a></li>
<li class="toctree-l1"><a class="reference internal" href="library.html">Using SLUG as a Library</a></li>
<li class="toctree-l1"><a class="reference internal" href="acknowledgements.html">Contributors and Acknowledgements</a></li>
</ul>

<div class="relations">
<h3>Related Topics</h3>
<ul>
  <li><a href="index.html">Documentation overview</a><ul>
      <li>Previous: <a href="intro.html" title="previous chapter">Introduction to SLUG</a></li>
      <li>Next: <a href="running.html" title="next chapter">Running a SLUG simulation</a></li>
  </ul></li>
</ul>
</div>








        </div>
      </div>
      <div class="clearer"></div>
    </div>
    <div class="footer">
      &#169;2014, Mark Krumholz, Michele Fumagalli, et al..
      
      |
      Powered by <a href="https://www.sphinx-doc.org/">Sphinx 8.1.3</a>
      &amp; <a href="https://alabaster.readthedocs.io">Alabaster 1.0.0</a>
      
      |
      <a href="_sources/compiling.rst.txt"
          rel="nofollow">Page source</a>
    </div>

    

    
  </body>
</html>