<!DOCTYPE html>

<html lang="en" data-content_root="./">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" /><meta name="viewport" content="width=device-width, initial-scale=1" />

    <title>sfr_slug: Bayesian Inference of Star Formation Rates &#8212; slug 2.0 documentation</title>
    <link rel="stylesheet" type="text/css" href="_static/pygments.css?v=fa44fd50" />
    <link rel="stylesheet" type="text/css" href="_static/basic.css?v=686e5160" />
    <link rel="stylesheet" type="text/css" href="_static/alabaster.css?v=27fed22d" />
    <script src="_static/documentation_options.js?v=60dbed4a"></script>
    <script src="_static/doctools.js?v=9bcbadda"></script>
    <script src="_static/sphinx_highlight.js?v=dc90522c"></script>
    <script async="async" src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>
    <link rel="index" title="Index" href="genindex.html" />
    <link rel="search" title="Search" href="search.html" />
    <link rel="next" title="Test Problems" href="tests.html" />
    <link rel="prev" title="cluster_slug: Bayesian Inference of Star Cluster Properties" href="cluster_slug.html" />
   
  <link rel="stylesheet" href="_static/custom.css" type="text/css" />
  

  
  

  </head><body>
  

    <div class="document">
      <div class="documentwrapper">
        <div class="bodywrapper">
          

          <div class="body" role="main">
            
  <section id="sfr-slug-bayesian-inference-of-star-formation-rates">
<span id="sec-sfr-slug"></span><h1>sfr_slug: Bayesian Inference of Star Formation Rates<a class="headerlink" href="#sfr-slug-bayesian-inference-of-star-formation-rates" title="Link to this heading">¶</a></h1>
<p>The slugy.sfr_slug module computes posterior probabilities on star formation rates given a set of star formation rates estimated using the “point mass estimate” (i.e., the estimate you would get for a fully sampled stellar population) for the SFR based on the ionizing, FUV, or bolometric luminosity. It is implemented as a wrapper around <a class="reference internal" href="bayesphot.html#sec-bayesphot"><span class="std std-ref">bayesphot: Bayesian Inference for Stochastic Stellar Populations</span></a>, so for details on how the calculation is performed see the bayesphot documentation.</p>
<section id="getting-the-default-library">
<h2>Getting the Default Library<a class="headerlink" href="#getting-the-default-library" title="Link to this heading">¶</a></h2>
<p>The sfr_slug module requires a pre-computed library of slug simulations to use as a “training set” for its calculations. Due to its size, the default library <em>is not</em> included in the slug git repository. Instead, it is provided for download from the <a class="reference external" href="http://www.slugsps.com/data">SLUG data products website</a>. Download the two files <code class="docutils literal notranslate"><span class="pre">SFR_SLUG_integrated_phot.fits</span></code> and <code class="docutils literal notranslate"><span class="pre">SFR_SLUG_integrated_prop.fits</span></code> and save them in the <code class="docutils literal notranslate"><span class="pre">sfr_slug</span></code> directory of the main respository. If you do not do so, and do not provide your own library when you attempt to use sfr_slug, you will be prompted to download the default library.</p>
</section>
<section id="basic-usage">
<h2>Basic Usage<a class="headerlink" href="#basic-usage" title="Link to this heading">¶</a></h2>
<p>The <code class="docutils literal notranslate"><span class="pre">sfr_slug/sfr_slug_example.py</span></code> file in the repository provides an example of how to use sfr_slug. Usage of is simple, as the functionality is all implemented through a single class, sfr_slug. The required steps are as follows:</p>
<ol class="arabic">
<li><p>Import the library and instantiate an <code class="docutils literal notranslate"><span class="pre">sfr_slug</span></code> object (see <a class="reference internal" href="#sec-sfr-slug-full"><span class="std std-ref">Full Documentation of slugpy.sfr_slug</span></a> for full details):</p>
<div class="highlight-rest notranslate"><div class="highlight"><pre><span></span>from slugpy.sfr_slug import sfr_slug
sfr_estimator = sfr_slug()
</pre></div>
</div>
</li>
</ol>
<p>This creates an sfr_slug object, using the default simulation library, $SLUG_DIR/sfr_slug/SFR_SLUG. If you have another library of simulations you’d rather use, you can use the <code class="docutils literal notranslate"><span class="pre">libname</span></code> keyword to the <code class="docutils literal notranslate"><span class="pre">sfr_slug</span></code> constructor to select it.</p>
<ol class="arabic" start="2">
<li><p>Specify your filter(s), for example:</p>
<div class="highlight-rest notranslate"><div class="highlight"><pre><span></span>sfr_estimator.add_filters(&#39;QH0&#39;)
</pre></div>
</div>
</li>
</ol>
<p>The <code class="docutils literal notranslate"><span class="pre">add_filter</span></code> method takes as an argument a string or list of strings specifying which filters you’re going to point mass SFRs based on. You can have more than one set of filters active at a time (just by calling <code class="docutils literal notranslate"><span class="pre">add_filters</span></code> more than once), and then specify which set of filters you’re using for any given calculation.</p>
<ol class="arabic" start="3">
<li><p>Specify your priors, for example:</p>
<div class="highlight-rest notranslate"><div class="highlight"><pre><span></span>sfr_estimator.priors = &#39;schechter&#39;
</pre></div>
</div>
</li>
</ol>
<p>The <code class="docutils literal notranslate"><span class="pre">priors</span></code> property specifies the assumed prior probability distribution on the star formation rate. It can be either <code class="docutils literal notranslate"><span class="pre">None</span></code> (in which case all simulations in the library are given equal prior probability), an array with as many elements as there are simulations in the library giving the prior for each one, a callable that takes a star formation rate as input and returns the prior for it, or a string whose value is either “flat” or “prior”. The two strings specify, respectively, a prior distribution that is either flat in log SFR or follows the Schechter function SFR distribution from <a class="reference external" href="http://adsabs.harvard.edu/abs/2011MNRAS.415.1815B">Bothwell et al. (2011)</a>:</p>
<div class="math notranslate nohighlight">
\[p(\log\mathrm{SFR}) \propto \mathrm{SFR}^{\alpha} \exp(-\mathrm{SFR}/\mathrm{SFR}_*)\]</div>
<p>with <span class="math notranslate nohighlight">\(\alpha = -0.51\)</span> and <span class="math notranslate nohighlight">\(\mathrm{SFR}_* = 9.2\,M_\odot\,\mathrm{yr}^{-1}\)</span>.</p>
<ol class="arabic" start="4">
<li><p>Generate the posterior probability distribuiton of SFR via:</p>
<div class="highlight-rest notranslate"><div class="highlight"><pre><span></span>logSFR, pdf = sfr_estimator.mpdf(logSFR_in, logSFRphoterr = logSFR_err)
</pre></div>
</div>
</li>
</ol>
<p>The argument <code class="docutils literal notranslate"><span class="pre">logSFR_in</span></code> can be a float or an array specifying one or more point mass estimates of the SFR in your chosen filter. For a case with two or more filters, then <code class="docutils literal notranslate"><span class="pre">logSFR_in</span></code> must be an array whose trailing dimension matches the number of filters. If you have added two or more filter sets, you need to specify which one you want to use via the <code class="docutils literal notranslate"><span class="pre">filters</span></code> keyword. The optional argument <code class="docutils literal notranslate"><span class="pre">logSFRphoterr</span></code> can be used to provide errors on the photometric SFRs. Like <code class="docutils literal notranslate"><span class="pre">logSFR_in</span></code>, it can be a float or an array.</p>
<p>The <code class="docutils literal notranslate"><span class="pre">sfr_slug.mpdf</span></code> method returns a tuple of two quantities. The first is a grid of log SFR values, and the second is the posterior probability distribution at each value of log SFR. If the input consisted of multiple photometric SFRs, the output will contains posterior probabilities for each input. The output grid will be created automatically be default, but all aspects of it (shape, size, placement of grid points) can be controlled by keywords – see <a class="reference internal" href="#sec-sfr-slug-full"><span class="std std-ref">Full Documentation of slugpy.sfr_slug</span></a>.</p>
</section>
<section id="full-documentation-of-slugpy-sfr-slug">
<span id="sec-sfr-slug-full"></span><h2>Full Documentation of slugpy.sfr_slug<a class="headerlink" href="#full-documentation-of-slugpy-sfr-slug" title="Link to this heading">¶</a></h2>
<dl class="py class">
<dt class="sig sig-object py" id="slugpy.sfr_slug.sfr_slug">
<em class="property"><span class="pre">class</span><span class="w"> </span></em><span class="sig-prename descclassname"><span class="pre">slugpy.sfr_slug.</span></span><span class="sig-name descname"><span class="pre">sfr_slug</span></span><span class="sig-paren">(</span><em class="sig-param"><span class="n"><span class="pre">libname</span></span><span class="o"><span class="pre">=</span></span><span class="default_value"><span class="pre">None</span></span></em>, <em class="sig-param"><span class="n"><span class="pre">detname</span></span><span class="o"><span class="pre">=</span></span><span class="default_value"><span class="pre">None</span></span></em>, <em class="sig-param"><span class="n"><span class="pre">filters</span></span><span class="o"><span class="pre">=</span></span><span class="default_value"><span class="pre">None</span></span></em>, <em class="sig-param"><span class="n"><span class="pre">bandwidth</span></span><span class="o"><span class="pre">=</span></span><span class="default_value"><span class="pre">0.1</span></span></em>, <em class="sig-param"><span class="n"><span class="pre">ktype</span></span><span class="o"><span class="pre">=</span></span><span class="default_value"><span class="pre">'gaussian'</span></span></em>, <em class="sig-param"><span class="n"><span class="pre">priors</span></span><span class="o"><span class="pre">=</span></span><span class="default_value"><span class="pre">None</span></span></em>, <em class="sig-param"><span class="n"><span class="pre">sample_density</span></span><span class="o"><span class="pre">=</span></span><span class="default_value"><span class="pre">'read'</span></span></em>, <em class="sig-param"><span class="n"><span class="pre">reltol</span></span><span class="o"><span class="pre">=</span></span><span class="default_value"><span class="pre">0.001</span></span></em>, <em class="sig-param"><span class="n"><span class="pre">abstol</span></span><span class="o"><span class="pre">=</span></span><span class="default_value"><span class="pre">1e-10</span></span></em>, <em class="sig-param"><span class="n"><span class="pre">leafsize</span></span><span class="o"><span class="pre">=</span></span><span class="default_value"><span class="pre">16</span></span></em><span class="sig-paren">)</span><a class="reference internal" href="_modules/slugpy/sfr_slug/sfr_slug.html#sfr_slug"><span class="viewcode-link"><span class="pre">[source]</span></span></a><a class="headerlink" href="#slugpy.sfr_slug.sfr_slug" title="Link to this definition">¶</a></dt>
<dd><p>A class that can be used to estimate the PDF of true star
formation rate from a set of input point mass estimates of the
star formation rate.</p>
<dl class="simple">
<dt>Properties</dt><dd><dl class="simple">
<dt>priors<span class="classifier">array, shape (N) | callable | ‘flat’ | ‘schechter’ | None</span></dt><dd><p>prior probability on each data point; interpretation
depends on the type passed; array, shape (N): values are
interpreted as the prior probability of each data point;
callable: the callable must take as an argument an array
of shape (N, nphys), and return an array of shape (N)
giving the prior probability at each data point; None:
all data points have equal prior probability; the values
‘flat’ and ‘schechter’ use priors p(log SFR) ~ constant and
p(log SFR) ~ SFR^alpha exp(-SFR/SFR_*), respectively, where
alpha = -0.51 and SFR_* = 9.2 Msun/yr are the values
measured by Bothwell et al. (2011)</p>
</dd>
<dt>bandwidth<span class="classifier">‘auto’ | array, shape (M)</span></dt><dd><p>bandwidth for kernel density estimation; if set to
‘auto’, the bandwidth will be estimated automatically</p>
</dd>
</dl>
</dd>
</dl>
<dl class="py method">
<dt class="sig sig-object py" id="slugpy.sfr_slug.sfr_slug.__init__">
<span class="sig-name descname"><span class="pre">__init__</span></span><span class="sig-paren">(</span><em class="sig-param"><span class="n"><span class="pre">libname</span></span><span class="o"><span class="pre">=</span></span><span class="default_value"><span class="pre">None</span></span></em>, <em class="sig-param"><span class="n"><span class="pre">detname</span></span><span class="o"><span class="pre">=</span></span><span class="default_value"><span class="pre">None</span></span></em>, <em class="sig-param"><span class="n"><span class="pre">filters</span></span><span class="o"><span class="pre">=</span></span><span class="default_value"><span class="pre">None</span></span></em>, <em class="sig-param"><span class="n"><span class="pre">bandwidth</span></span><span class="o"><span class="pre">=</span></span><span class="default_value"><span class="pre">0.1</span></span></em>, <em class="sig-param"><span class="n"><span class="pre">ktype</span></span><span class="o"><span class="pre">=</span></span><span class="default_value"><span class="pre">'gaussian'</span></span></em>, <em class="sig-param"><span class="n"><span class="pre">priors</span></span><span class="o"><span class="pre">=</span></span><span class="default_value"><span class="pre">None</span></span></em>, <em class="sig-param"><span class="n"><span class="pre">sample_density</span></span><span class="o"><span class="pre">=</span></span><span class="default_value"><span class="pre">'read'</span></span></em>, <em class="sig-param"><span class="n"><span class="pre">reltol</span></span><span class="o"><span class="pre">=</span></span><span class="default_value"><span class="pre">0.001</span></span></em>, <em class="sig-param"><span class="n"><span class="pre">abstol</span></span><span class="o"><span class="pre">=</span></span><span class="default_value"><span class="pre">1e-10</span></span></em>, <em class="sig-param"><span class="n"><span class="pre">leafsize</span></span><span class="o"><span class="pre">=</span></span><span class="default_value"><span class="pre">16</span></span></em><span class="sig-paren">)</span><a class="reference internal" href="_modules/slugpy/sfr_slug/sfr_slug.html#sfr_slug.__init__"><span class="viewcode-link"><span class="pre">[source]</span></span></a><a class="headerlink" href="#slugpy.sfr_slug.sfr_slug.__init__" title="Link to this definition">¶</a></dt>
<dd><blockquote>
<div><p>Initialize an sfr_slug object.</p>
</div></blockquote>
<dl>
<dt>Parameters</dt><dd><blockquote>
<div><dl class="simple">
<dt>libname<span class="classifier">string</span></dt><dd><p>name of the SLUG model to load; if left as None, the default
is $SLUG_DIR/sfr_slug/SFR_SLUG</p>
</dd>
<dt>detname<span class="classifier">string</span></dt><dd><p>name of a SLUG model run with the same parameters but no
stochasticity; used to establish the non-stochastic
photometry to SFR conversions; if left as None, the default
is libname_DET</p>
</dd>
<dt>filters<span class="classifier">iterable of stringlike</span></dt><dd><p>list of filter names to be used for inferenence</p>
</dd>
<dt>bandwidth<span class="classifier">‘auto’ | float | array, shape (M)</span></dt><dd><p>bandwidth for kernel density estimation; if set to
‘auto’, the bandwidth will be estimated automatically;
if set to a float, the same bandwidth is used in all
dimensions</p>
</dd>
<dt>ktype<span class="classifier">string</span></dt><dd><p>type of kernel to be used in densty estimation; allowed
values are ‘gaussian’ (default), ‘epanechnikov’, and
‘tophat’; only Gaussian can be used with error bars</p>
</dd>
<dt>priors<span class="classifier">array, shape (N) | callable | None</span></dt><dd><p>prior probability on each data point; interpretation
depends on the type passed; array, shape (N): values are
interpreted as the prior probability of each data point;
callable: the callable must take as an argument an array
of shape (N, nphys), and return an array of shape (N)
giving the prior probability at each data point; None:
all data points have equal prior probability</p>
</dd>
<dt>sample_density<span class="classifier">array, shape (N) | callable | ‘auto’ | ‘read’ | None</span></dt><dd><p>the density of the data samples at each data point; this
need not match the prior density; interpretation depends
on the type passed; array, shape (N): values are
interpreted as the density of data sampling at each
sample point; callable: the callable must take as an
argument an array of shape (N, nphys), and return an
array of shape (N) giving the sampling density at each
point; ‘auto’: the sample density will be computed
directly from the data set; note that this can be quite
slow for large data sets, so it is preferable to specify
this analytically if it is known; ‘read’: the sample
density is to be read from a numpy save file whose name
matches that of the library, with the extension _density.npy
added; None: data are assumed to be uniformly sampled</p>
</dd>
<dt>reltol<span class="classifier">float</span></dt><dd><p>relative error tolerance; errors on all returned
probabilities p will satisfy either
abs(p_est - p_true) &lt;= reltol * p_est   OR
abs(p_est - p_true) &lt;= abstol,
where p_est is the returned estimate and p_true is the
true value</p>
</dd>
<dt>abstol<span class="classifier">float</span></dt><dd><p>absolute error tolerance; see above</p>
</dd>
<dt>leafsize<span class="classifier">int</span></dt><dd><p>number of data points in each leaf of the KD tree</p>
</dd>
</dl>
</div></blockquote>
<dl class="simple">
<dt>Returns</dt><dd><p>Nothing</p>
</dd>
<dt>Raises</dt><dd><p>IOError, if the library cannot be found</p>
</dd>
</dl>
</dd>
</dl>
</dd></dl>

<dl class="py attribute">
<dt class="sig sig-object py" id="slugpy.sfr_slug.sfr_slug.__weakref__">
<span class="sig-name descname"><span class="pre">__weakref__</span></span><a class="headerlink" href="#slugpy.sfr_slug.sfr_slug.__weakref__" title="Link to this definition">¶</a></dt>
<dd><p>list of weak references to the object</p>
</dd></dl>

<dl class="py method">
<dt class="sig sig-object py" id="slugpy.sfr_slug.sfr_slug.add_filters">
<span class="sig-name descname"><span class="pre">add_filters</span></span><span class="sig-paren">(</span><em class="sig-param"><span class="n"><span class="pre">filters</span></span></em><span class="sig-paren">)</span><a class="reference internal" href="_modules/slugpy/sfr_slug/sfr_slug.html#sfr_slug.add_filters"><span class="viewcode-link"><span class="pre">[source]</span></span></a><a class="headerlink" href="#slugpy.sfr_slug.sfr_slug.add_filters" title="Link to this definition">¶</a></dt>
<dd><p>Add a set of filters to use for cluster property estimation</p>
<dl class="simple">
<dt>Parameters</dt><dd><dl class="simple">
<dt>filters<span class="classifier">iterable of stringlike</span></dt><dd><p>list of filter names to be used for inferenence</p>
</dd>
</dl>
</dd>
<dt>Returns</dt><dd><p>nothing</p>
</dd>
</dl>
</dd></dl>

<dl class="py method">
<dt class="sig sig-object py" id="slugpy.sfr_slug.sfr_slug.filters">
<span class="sig-name descname"><span class="pre">filters</span></span><span class="sig-paren">(</span><span class="sig-paren">)</span><a class="reference internal" href="_modules/slugpy/sfr_slug/sfr_slug.html#sfr_slug.filters"><span class="viewcode-link"><span class="pre">[source]</span></span></a><a class="headerlink" href="#slugpy.sfr_slug.sfr_slug.filters" title="Link to this definition">¶</a></dt>
<dd><p>Returns list of all available filters</p>
<dl class="simple">
<dt>Parameters:</dt><dd><p>None</p>
</dd>
<dt>Returns:</dt><dd><dl class="simple">
<dt>filters<span class="classifier">list of strings</span></dt><dd><p>list of available filter names</p>
</dd>
</dl>
</dd>
</dl>
</dd></dl>

<dl class="py method">
<dt class="sig sig-object py" id="slugpy.sfr_slug.sfr_slug.logL">
<span class="sig-name descname"><span class="pre">logL</span></span><span class="sig-paren">(</span><em class="sig-param"><span class="n"><span class="pre">logSFR</span></span></em>, <em class="sig-param"><span class="n"><span class="pre">logSFRphot</span></span></em>, <em class="sig-param"><span class="n"><span class="pre">logSFRphoterr</span></span><span class="o"><span class="pre">=</span></span><span class="default_value"><span class="pre">None</span></span></em>, <em class="sig-param"><span class="n"><span class="pre">filters</span></span><span class="o"><span class="pre">=</span></span><span class="default_value"><span class="pre">None</span></span></em><span class="sig-paren">)</span><a class="reference internal" href="_modules/slugpy/sfr_slug/sfr_slug.html#sfr_slug.logL"><span class="viewcode-link"><span class="pre">[source]</span></span></a><a class="headerlink" href="#slugpy.sfr_slug.sfr_slug.logL" title="Link to this definition">¶</a></dt>
<dd><p>This function returns the natural log of the likelihood
function evaluated at a particular log SFR and set of log
luminosities</p>
<dl class="simple">
<dt>Parameters:</dt><dd><dl class="simple">
<dt>logSFR<span class="classifier">float or arraylike</span></dt><dd><p>float or array giving values of the log SFR; for an
array, the operation is vectorized</p>
</dd>
<dt>logSFRphot<span class="classifier">float or arraylike, shape (nfilter) or (…, nfilter)</span></dt><dd><p>float or array giving the SFR inferred from photometry using a
deterministic conversion; for an array, the operation is
vectorized over the leading dimensions</p>
</dd>
<dt>logSFRphoterr<span class="classifier">float arraylike, shape (nfilter) or (…, nfilter)</span></dt><dd><p>float or array giving photometric SFR errors; for a
multidimensional array, the operation is vectorized over
the leading dimensions</p>
</dd>
<dt>filters<span class="classifier">listlike of strings</span></dt><dd><p>list of photometric filters used for the SFR estimation;
if left as None, and only 1 set of photometric filters
has been defined for the sfr_slug object, that set will
be used by default</p>
</dd>
</dl>
</dd>
<dt>Returns:</dt><dd><dl class="simple">
<dt>logL<span class="classifier">float or arraylike</span></dt><dd><p>natural log of the likelihood function</p>
</dd>
</dl>
</dd>
</dl>
</dd></dl>

<dl class="py method">
<dt class="sig sig-object py" id="slugpy.sfr_slug.sfr_slug.mcmc">
<span class="sig-name descname"><span class="pre">mcmc</span></span><span class="sig-paren">(</span><em class="sig-param"><span class="n"><span class="pre">photprop</span></span></em>, <em class="sig-param"><span class="n"><span class="pre">photerr</span></span><span class="o"><span class="pre">=</span></span><span class="default_value"><span class="pre">None</span></span></em>, <em class="sig-param"><span class="n"><span class="pre">mc_walkers</span></span><span class="o"><span class="pre">=</span></span><span class="default_value"><span class="pre">100</span></span></em>, <em class="sig-param"><span class="n"><span class="pre">mc_steps</span></span><span class="o"><span class="pre">=</span></span><span class="default_value"><span class="pre">500</span></span></em>, <em class="sig-param"><span class="n"><span class="pre">mc_burn_in</span></span><span class="o"><span class="pre">=</span></span><span class="default_value"><span class="pre">50</span></span></em>, <em class="sig-param"><span class="n"><span class="pre">filters</span></span><span class="o"><span class="pre">=</span></span><span class="default_value"><span class="pre">None</span></span></em><span class="sig-paren">)</span><a class="reference internal" href="_modules/slugpy/sfr_slug/sfr_slug.html#sfr_slug.mcmc"><span class="viewcode-link"><span class="pre">[source]</span></span></a><a class="headerlink" href="#slugpy.sfr_slug.sfr_slug.mcmc" title="Link to this definition">¶</a></dt>
<dd><p>This function returns a sample of MCMC walkers for log SFR</p>
<dl class="simple">
<dt>Parameters:</dt><dd><dl class="simple">
<dt>photprop<span class="classifier">arraylike, shape (nfilter) or (…, nfilter)</span></dt><dd><p>array giving the photometric values; for a
multidimensional array, the operation is vectorized over
the leading dimensions</p>
</dd>
<dt>photerr<span class="classifier">arraylike, shape (nfilter) or (…, nfilter)</span></dt><dd><p>array giving photometric errors; for a multidimensional
array, the operation is vectorized over the leading
dimensions</p>
</dd>
<dt>mc_walkers<span class="classifier">int</span></dt><dd><p>number of walkers to use in the MCMC</p>
</dd>
<dt>mc_steps<span class="classifier">int</span></dt><dd><p>number of steps in the MCMC</p>
</dd>
<dt>mc_burn_in<span class="classifier">int</span></dt><dd><p>number of steps to consider “burn-in” and discard</p>
</dd>
<dt>filters<span class="classifier">listlike of strings</span></dt><dd><p>list of photometric filters to use; if left as None, and
only 1 set of photometric filters has been defined for
the cluster_slug object, that set will be used by
default</p>
</dd>
</dl>
</dd>
<dt>Returns</dt><dd><dl class="simple">
<dt>samples<span class="classifier">array</span></dt><dd><p>array of sample points returned by the MCMC</p>
</dd>
</dl>
</dd>
</dl>
</dd></dl>

<dl class="py method">
<dt class="sig sig-object py" id="slugpy.sfr_slug.sfr_slug.mpdf">
<span class="sig-name descname"><span class="pre">mpdf</span></span><span class="sig-paren">(</span><em class="sig-param"><span class="n"><span class="pre">logSFRphot</span></span></em>, <em class="sig-param"><span class="n"><span class="pre">logSFRphoterr</span></span><span class="o"><span class="pre">=</span></span><span class="default_value"><span class="pre">None</span></span></em>, <em class="sig-param"><span class="n"><span class="pre">ngrid</span></span><span class="o"><span class="pre">=</span></span><span class="default_value"><span class="pre">128</span></span></em>, <em class="sig-param"><span class="n"><span class="pre">qmin</span></span><span class="o"><span class="pre">=</span></span><span class="default_value"><span class="pre">None</span></span></em>, <em class="sig-param"><span class="n"><span class="pre">qmax</span></span><span class="o"><span class="pre">=</span></span><span class="default_value"><span class="pre">None</span></span></em>, <em class="sig-param"><span class="n"><span class="pre">grid</span></span><span class="o"><span class="pre">=</span></span><span class="default_value"><span class="pre">None</span></span></em>, <em class="sig-param"><span class="n"><span class="pre">norm</span></span><span class="o"><span class="pre">=</span></span><span class="default_value"><span class="pre">True</span></span></em>, <em class="sig-param"><span class="n"><span class="pre">filters</span></span><span class="o"><span class="pre">=</span></span><span class="default_value"><span class="pre">None</span></span></em><span class="sig-paren">)</span><a class="reference internal" href="_modules/slugpy/sfr_slug/sfr_slug.html#sfr_slug.mpdf"><span class="viewcode-link"><span class="pre">[source]</span></span></a><a class="headerlink" href="#slugpy.sfr_slug.sfr_slug.mpdf" title="Link to this definition">¶</a></dt>
<dd><p>Returns the marginal probability of log SFR for one or more
input sets of photometric properties. Output quantities are
computed on a grid of values, in the same style as meshgrid</p>
<dl class="simple">
<dt>Parameters:</dt><dd><dl class="simple">
<dt>logSFRphot<span class="classifier">float or arraylike</span></dt><dd><p>float or array giving the log SFR inferred from
photometry using a deterministic conversion; if the
argument is an array, the operation is vectorized over
it</p>
</dd>
<dt>logSFRphoterr<span class="classifier">arraylike, shape (nfilter) or (…, nfilter)</span></dt><dd><p>array giving photometric errors; for a multidimensional
array, the operation is vectorized over the leading
dimensions</p>
</dd>
<dt>ngrid<span class="classifier">int</span></dt><dd><p>number of points in the output log SFR grid</p>
</dd>
<dt>qmin<span class="classifier">float</span></dt><dd><p>minimum value in the output log SFR grid</p>
</dd>
<dt>qmax<span class="classifier">float</span></dt><dd><p>maximum value in the output log SFR grid</p>
</dd>
<dt>grid<span class="classifier">array</span></dt><dd><p>set of values defining the grid of SFR values at which
to evaluate; if set, overrides ngrid, qmin, and qmax</p>
</dd>
<dt>norm<span class="classifier">bool</span></dt><dd><p>if True, returned pdf’s will be normalized to integrate
to 1</p>
</dd>
<dt>filters<span class="classifier">listlike of strings</span></dt><dd><p>list of photometric filters to use; if left as None, and
only 1 set of photometric filters has been defined for
the cluster_slug object, that set will be used by
default</p>
</dd>
</dl>
</dd>
<dt>Returns:</dt><dd><dl class="simple">
<dt>grid_out<span class="classifier">array</span></dt><dd><p>array of log SFR values at which the PDF is evaluated</p>
</dd>
<dt>pdf<span class="classifier">array</span></dt><dd><p>array of marginal posterior probabilities at each point
of the output grid, for each input photometric value;
the leading dimensions match the leading dimensions
produced by broadcasting the leading dimensions of
photprop and photerr together, while the trailing
dimensions match the dimensions of the output grid</p>
</dd>
</dl>
</dd>
</dl>
</dd></dl>

</dd></dl>

</section>
</section>


          </div>
          
        </div>
      </div>
      <div class="sphinxsidebar" role="navigation" aria-label="Main">
        <div class="sphinxsidebarwrapper">
<h1 class="logo"><a href="index.html">slug</a></h1>









<search id="searchbox" style="display: none" role="search">
    <div class="searchformwrapper">
    <form class="search" action="search.html" method="get">
      <input type="text" name="q" aria-labelledby="searchlabel" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" placeholder="Search"/>
      <input type="submit" value="Go" />
    </form>
    </div>
</search>
<script>document.getElementById('searchbox').style.display = "block"</script><h3>Navigation</h3>
<ul class="current">
<li class="toctree-l1"><a class="reference internal" href="license.html">License</a></li>
<li class="toctree-l1"><a class="reference internal" href="getting.html">Getting SLUG</a></li>
<li class="toctree-l1"><a class="reference internal" href="intro.html">Introduction to SLUG</a></li>
<li class="toctree-l1"><a class="reference internal" href="compiling.html">Compiling and Installing SLUG</a></li>
<li class="toctree-l1"><a class="reference internal" href="running.html">Running a SLUG simulation</a></li>
<li class="toctree-l1"><a class="reference internal" href="parameters.html">Parameter Specification</a></li>
<li class="toctree-l1"><a class="reference internal" href="pdfs.html">Probability Distribution Functions</a></li>
<li class="toctree-l1"><a class="reference internal" href="output.html">Output Files and Format</a></li>
<li class="toctree-l1"><a class="reference internal" href="filters.html">Filters and Filter Data</a></li>
<li class="toctree-l1"><a class="reference internal" href="slugpy.html">slugpy – The Python Helper Library</a></li>
<li class="toctree-l1"><a class="reference internal" href="cloudy.html">cloudy_slug: An Automated Interface to cloudy</a></li>
<li class="toctree-l1"><a class="reference internal" href="bayesphot.html">bayesphot: Bayesian Inference for Stochastic Stellar Populations</a></li>
<li class="toctree-l1"><a class="reference internal" href="cluster_slug.html">cluster_slug: Bayesian Inference of Star Cluster Properties</a></li>
<li class="toctree-l1 current"><a class="current reference internal" href="#">sfr_slug: Bayesian Inference of Star Formation Rates</a><ul>
<li class="toctree-l2"><a class="reference internal" href="#getting-the-default-library">Getting the Default Library</a></li>
<li class="toctree-l2"><a class="reference internal" href="#basic-usage">Basic Usage</a></li>
<li class="toctree-l2"><a class="reference internal" href="#full-documentation-of-slugpy-sfr-slug">Full Documentation of slugpy.sfr_slug</a></li>
</ul>
</li>
<li class="toctree-l1"><a class="reference internal" href="tests.html">Test Problems</a></li>
<li class="toctree-l1"><a class="reference internal" href="library.html">Using SLUG as a Library</a></li>
<li class="toctree-l1"><a class="reference internal" href="acknowledgements.html">Contributors and Acknowledgements</a></li>
</ul>

<div class="relations">
<h3>Related Topics</h3>
<ul>
  <li><a href="index.html">Documentation overview</a><ul>
      <li>Previous: <a href="cluster_slug.html" title="previous chapter">cluster_slug: Bayesian Inference of Star Cluster Properties</a></li>
      <li>Next: <a href="tests.html" title="next chapter">Test Problems</a></li>
  </ul></li>
</ul>
</div>








        </div>
      </div>
      <div class="clearer"></div>
    </div>
    <div class="footer">
      &#169;2014, Mark Krumholz, Michele Fumagalli, et al..
      
      |
      Powered by <a href="https://www.sphinx-doc.org/">Sphinx 8.1.3</a>
      &amp; <a href="https://alabaster.readthedocs.io">Alabaster 1.0.0</a>
      
      |
      <a href="_sources/sfr_slug.rst.txt"
          rel="nofollow">Page source</a>
    </div>

    

    
  </body>
</html>